import {Component} from '@angular/core';
import {Storage} from "@ionic/storage";


@Component({
    selector: 'page-store',
    templateUrl: 'store.html',
})
export class StorePage {
    ultimoBotaoPressionado: any;

    constructor(private storage: Storage) {
    }

    ionViewDidLoad() {
        this.storage.get("ultimoBotaoPressionado").then((val) => {
            this.ultimoBotaoPressionado = val;
        });
    }

    Botao1() {
        //usando componete ionic
        this.storage.set('ultimoBotaoPressionado', 1);
        //usando javascrip puro para local store
        localStorage.setItem('opção', '1');
        //informação fica temporaria com java script
        sessionStorage.setItem('sessão', '1');

    }

    Botao2() {
        this.storage.set('ultimoBotaoPressionado', 2);
        //usando java scrip puro para local store
        localStorage.setItem('opção', '2');

    }

}
