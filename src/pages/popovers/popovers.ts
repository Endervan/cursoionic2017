import {Component} from '@angular/core';
import { PopoverController} from 'ionic-angular';
import {IconesPage} from "../icones/icones";


@Component({
    selector: 'page-popovers',
    templateUrl: 'popovers.html',
})
export class PopoversPage {

    constructor(public popoversCtrl: PopoverController) {
    }

    ionViewDidLoad() {

    }

    //let = cria variavel
    AbrirPopovers() {
        let popover = this.popoversCtrl.create(IconesPage);

        popover.present();
    }

}
