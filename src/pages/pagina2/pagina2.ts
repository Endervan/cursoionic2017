import {Component} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';


@Component({
    selector: 'page-pagina2',
    templateUrl: 'pagina2.html',
})
export class Pagina2Page {
    usuario: any = {nome: '', idade: ''};

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.usuario = this.navParams.get("usuario");

        console.log(this.usuario);
    }

    ionViewDidLoad() {
    }


    fecharPagina() {
        this.navCtrl.pop();//fechar a pagina
        //this.navCtrl.popAll();//fecha seguência das paginas
        //this.navCtrl.popToRoot();//volta para paginas principal
    }
}
