import {Component} from '@angular/core';
import { LoadingController} from 'ionic-angular';


@Component({
    selector: 'page-loading',
    templateUrl: 'loading.html',
})
export class LoadingPage {

    constructor(public  loadingCtrl: LoadingController) {
    }

    ionViewDidLoad() {
        let aguarde = this.loadingCtrl.create({
            content: "Por favor aguarde",
            duration: 1000
        });

        aguarde.present();
    }

    salvar() {
        let aguarde = this.loadingCtrl.create({
            content: "Salvado dados",
        });
        aguarde.present();

        //processa algo
        setTimeout(() => {
            console.log('Dados Salvos com Sucesso');
            aguarde.dismiss();//terminado loading
        }, 5000);

    }

    salvarCustomizado() {
        let aguardeCustomizado = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<div class="custom-spinner-container">
                <div class = "custom-spinner-box" ></div>
                </div>`,
            duration: 5000

        });

        aguardeCustomizado.onDidDismiss(() => {
            alert('Aguarde foi encerrado');
        });


        aguardeCustomizado.present();

    }

    salvarSemSpinner(){
        let aguarde = this.loadingCtrl.create({
            spinner: 'hide',
            content:"Por favor Aguarde",
        });

        aguarde.present();
        //processa algo
        setTimeout(() => {
            console.log('Dados Salvos com Sucesso');
            aguarde.dismiss();//terminado loading
        }, 5000);
    }

    salvarComSpinnerDiferente(){
        let aguarde = this.loadingCtrl.create({
            spinner: 'dots',//hide,dots,ios,bubbles,circles,crescent
            content:"Por favor Aguarde",
            duration : 2000
        });

        aguarde.present();
    }


}
