import { Component } from '@angular/core';
import { ToastController} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@Component({
  selector: 'page-barcode',
  templateUrl: 'barcode.html',
})
export class BarcodePage {

  constructor(private barcodeScanner : BarcodeScanner,private  toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

    ScannearProdutos() {
        this.barcodeScanner.scan().then(barcodeData => {
            let response: string = JSON.stringify(barcodeData);
            this.toastCtrl.create({
                message: response,
                duration: 2000,
                position: 'top'
            }).present();
        }, (err) => {
            let erro = JSON.stringify(err);//transformando (err) em string
            this.toastCtrl.create({
                message: erro,
                duration: 2000,
                position: 'top'
            }).present();
        });
    }


}
