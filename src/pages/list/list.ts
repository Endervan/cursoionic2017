import { Component } from '@angular/core';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  itens :string[] = [
      'laranja','Maça','Abacate','Manga','Pera','Uva','Limão','Banana'
  ];

  constructor() {
  }

  ionViewDidLoad() {
  }

    selecioneItem(item){
        console.log(item);
    }

}
