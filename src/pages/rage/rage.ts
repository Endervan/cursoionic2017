import { Component } from '@angular/core';

@Component({
  selector: 'page-rage',
  templateUrl: 'rage.html',
})
export class RagePage {
    brilhodaTela : number = 100;
    Taxa : number = 1000;
    faixaEtaria : any = {lower:16,upper:60};
  constructor() {
  }

  ionViewDidLoad() {
  }

}
