import {Component} from '@angular/core';
import { ModalController, ToastController} from 'ionic-angular';
import {FabPage} from "../fab/fab";
import {ModalConteudoPage} from "../modal-conteudo/modal-conteudo";

@Component({
    selector: 'page-modal-principal',
    templateUrl: 'modal-principal.html',
})
export class ModalPrincipalPage {

    constructor(public  modalCtrl: ModalController, public  toastCtrl: ToastController) {
    }

    ionViewDidLoad() {
    }

    abrirModalBasico() {
        this.modalCtrl.create(FabPage).present();
    }

    abrirModalComParamentros() {
        let param = {nome: "Ender", idade: 29}
        this.modalCtrl.create(ModalConteudoPage, {usuario: param}).present();

    }

    abrirModalComBotaoClose() {
        let modal = this.modalCtrl.create(ModalConteudoPage);

        modal.onDidDismiss(data => {
            this.toastCtrl.create({
                message : data,
                duration : 2000,
                position : 'top'
            }).present();
        });

        modal.present();
    }
}
