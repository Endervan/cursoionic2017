import {Component, ViewChild} from '@angular/core';
import { Slides, ToastController} from 'ionic-angular';


@Component({
  selector: 'page-slides',
  templateUrl: 'slides.html',
})
export class SlidesPage {
@ViewChild(Slides) slides : Slides;
  constructor(private toastCtrl : ToastController) {
  }

  ionViewDidLoad() {
  }

    sliderAlterado(){
    let currentIndex = this.slides.getActiveIndex();

        this.toastCtrl.create({
            message: 'Slider ' + currentIndex + ' foi Selecionado',
            duration : 2000,
            position : 'top'
        }).present();

    }

    avancarSlider(){
        this.slides.slideTo(2,2000)
    }

}
