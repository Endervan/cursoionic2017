import {Component} from '@angular/core';


@Component({
    selector: 'page-datetime',
    templateUrl: 'datetime.html',
})
export class DatetimePage {
    event: any = {
        month: '2018-01-06',
        timeStarts: '07:43',
        timeEnds: '2017-01-07'
    }

    constructor() {
    }

    ionViewDidLoad() {
    }

}
