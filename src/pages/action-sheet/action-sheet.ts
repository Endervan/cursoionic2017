import { Component } from '@angular/core';
import {ActionSheetController} from 'ionic-angular';



@Component({
  selector: 'page-action-sheet',
  templateUrl: 'action-sheet.html',
})
export class ActionSheetPage {

  constructor(public actionSheetCtrl : ActionSheetController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActionSheetPage');
  }

    AbrirActionSheet(){
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Opções',
            buttons: [
                {
                    icon:'create',
                    text: 'Opção A',
                    role: 'destructive',
                    handler: () => {
                        alert('Você Clicou na opção A')
                    }
                },{
                    icon:'create',
                    text: 'Opção B',
                    handler: () => {
                        alert('Você Clicou na opção B')
                    }
                },{
                    icon:'exit',
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        alert('Você clicou em  Cancelando ')
                    }
                }
            ]
        });
        actionSheet.present();//so mostra se usa esse Present
    }
}
