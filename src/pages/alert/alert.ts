import {Component} from '@angular/core';
import {AlertController} from 'ionic-angular';


@Component({
    selector: 'page-alert',
    templateUrl: 'alert.html',
})
export class AlertPage {

    constructor(private alertCtrl: AlertController) {
    }

    ionViewDidLoad() {
    }


    BasicAlert() {
        let alert = this.alertCtrl.create({
            title: 'Atenção',
            subTitle: 'Curso Ionic em Promoção',
            buttons: ['Dismiss']
        });
        alert.present();
    }

    ConfirmAlert() {
        let alert = this.alertCtrl.create({
            title: 'Confirma Compra!',
            subTitle: 'Tei Certeza que deseja Compra este Livro?',
            buttons: [
                {
                    text: "Cancelar",
                    role: 'cancel',
                    handler: () => {
                        console.log("Compra Cancelada");
                    }
                },
                {
                    text: "Comprar",
                    handler: () => {
                        console.log("Compra Realizada com sucesso");
                    }
                }
            ]
        });
        alert.present();
    }

    InputAlert() {
        let alert = this.alertCtrl.create({
            title: 'Acesso Restrito',
            inputs: [
                {
                    name: 'login',
                    placeholder: 'Login',
                },
                {
                    name: 'senha',
                    placeholder: 'Senha',
                    type: 'password'
                }

            ],
            buttons: [
                {
                    text: "Cancelar",
                    role: 'cancel',
                    handler: data => {
                        console.log('Autenticacao cancelada');
                    }
                },
                {
                    text: "Entra",
                    handler: data => {
                        if (data.login == 'ender' && data.senha == '123') {
                            console.log('Usuario Autenticado');
                        } else {
                            console.log('Dados nao confere');

                        }
                    }
                }
            ]

        });
        alert.present();
    }

    RadioAlert() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Escolha uma cor ?');
        alert.addInput(
            {
                type: 'radio',
                label: 'Azul',
                value: 'blue',
                checked: true
            }
        );
        alert.addInput(
            {
                type: 'radio',
                label: 'vermelho',
                value: 'red',
                checked: false
            }
        );
        alert.addInput(
            {
                type: 'radio',
                label: 'Verde',
                value: 'green',
                checked: false
            }
        );

        alert.addButton("Cancelar");
        alert.addButton({

            text: 'OK',
            handler: (data: any) => {
                console.log('Cor Selecionada', data);
            }
        });

        alert.present();

    }


    checkAlert(){
        let alert = this.alertCtrl.create();
        alert.setTitle("Quais frutas voce gosta???");

        alert.addInput({
            type: 'checkbox',
            label: 'Laranja',
            value: 'laranja',
            checked: true
        });

        alert.addInput({
            type: 'checkbox',
            label: 'Banana',
            value: 'banana',
            checked: false
        });

        alert.addInput({
            type: 'checkbox',
            label: 'Maça',
            value: 'maca',
            checked: false
        });

        alert.addButton("Cancelar");
        alert.addButton({
            text: 'OK',
            handler: (data: any) => {
                console.log('Voce Selecionou as Frutas', data);
            }
        });

        alert.present();

    }


}
