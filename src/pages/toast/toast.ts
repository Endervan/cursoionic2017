import {Component} from '@angular/core';
import { ToastController} from 'ionic-angular';


@Component({
    selector: 'page-toast',
    templateUrl: 'toast.html',
})
export class ToastPage {

    constructor(public toastCrlt: ToastController) {
    }


    ionViewDidLoad() {
    }


    showToast(position: string) {
        this.toastCrlt.create({
            message: 'Dados salvo com Sucesso',
            duration: 2000,
            position: position
        }).present();

    }

    showLongToast(){
        this.toastCrlt.create({
            message: 'Seja bem vindo curso de ionic aqui tera oportunidade para aprender com toast long',
            duration: 2000,
            position: 'top'
        }).present();
    }

    showToastWithCloseButtom(){
        this.toastCrlt.create({
            message: 'Operação realizada com sucesso',
            showCloseButton :true,
            closeButtonText : 'X',
            position : 'top'
        }).present();
    }

}
