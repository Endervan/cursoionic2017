import {Component} from '@angular/core';
import { ToastController} from 'ionic-angular';
import {CameraOptions, Camera} from "@ionic-native/camera";


@Component({
    selector: 'page-camera',
    templateUrl: 'camera.html',
})
export class CameraPage {

    foto: any;

    constructor(private  camera: Camera, private  toastCtrl: ToastController) {
    }

    ionViewDidLoad() {
    }

    tiraFoto() {
        const options: CameraOptions = {

            quality: 100,//qualidade
            destinationType: this.camera.DestinationType.DATA_URL,//destino ######### DATA_URL->APRESENTA FOTO
            encodingType: this.camera.EncodingType.JPEG,//TIPO ARQUIVO jpGE,png ETC..
            mediaType: this.camera.MediaType.PICTURE//TIPO CARA VAI TIRA EX:FOTO,VIDEO ETC..

        };

        this.camera.getPicture(options).then((imageData) => {

            let base64Image = 'data:image/jpeg;base64,' + imageData;

            this.foto = base64Image;

        }, (err) => {

            this.toastCtrl.create({
                message: 'Não foi Possivel Tira a Foto !',
                duration: 2000,
                position: 'top'
            }).present();

        });

    }
}