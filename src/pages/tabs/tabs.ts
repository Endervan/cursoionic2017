import { ToastPage } from './../toast/toast';
import { CardPage } from './../card/card';
import { BotoesPage } from './../botoes/botoes';
import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  botoesPage = BotoesPage;
  CardPage = CardPage;
  ToastPage = ToastPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

}
