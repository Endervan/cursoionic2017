import {Component} from '@angular/core';
import { LoadingController, ToastController} from 'ionic-angular';
import {CepProvider} from "../../providers/cep/cep";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
    selector: 'page-cep',
    templateUrl: 'cep.html',
})
export class CepPage {
    public form: FormGroup;
    response: any;

    constructor(private  cepProvider: CepProvider, private fb: FormBuilder, private  toastCrlt: ToastController, private loadingCtrl: LoadingController) {
        this.form = this.fb.group({
            cep: ['', Validators.compose([
                Validators.required
            ])],
        });
    }

    ionViewDidLoad() {
    }


    consultarEnderecoPeloCep() {

        let aguarde = this.loadingCtrl.create({
            spinner: 'dots',//hide,dots,ios,bubbles,circles,crescent
            content: "Processando",
        });
        aguarde.present();

        let valueCep = this.form.controls['cep'].value;
        this.cepProvider.ListarEndereco(valueCep).then((data) => {
            this.response = data.json();


            aguarde.dismiss();


        }).catch((response) => {

            this.toastCrlt.create({
                message: 'Cep não Encontrado',
                duration: 2000,
                position: 'top'
            }).present();

            aguarde.dismiss();
        });
    }
}
