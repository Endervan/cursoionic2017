import {Component} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation';

@Component({
    selector: 'page-geolocalizacao',
    templateUrl: 'geolocalizacao.html',
})


// protocolo de navegacao por gps no servidor e obrigatorio usa https
export class GeolocalizacaoPage {
    latitude: number;
    longitude: number;

    constructor(private  geolocation: Geolocation) {
    }

    ionViewDidLoad() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.latitude = resp.coords.latitude;
            this.longitude = resp.coords.longitude;
        }).catch((error) => {
            console.log('Error getting location', error);
        });

        let watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    }


}
