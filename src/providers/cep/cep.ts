import {Injectable} from '@angular/core';
import {Headers,Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';



//injetando api cpc desse site https://viacep.com.br/
@Injectable()
export class CepProvider {

    constructor(private http: Http) {

    }

    ListarEndereco(cep: number) {
        let url = "https://viacep.com.br/ws/"+  cep +"/json/";

        let headers = new Headers();
        headers.append('Content-type', 'application/json');
        //headers.append('Authorization','Bearer'+localStorage.getItem(alertBrasil))//nao precisa pq api publica
        return this.http.get(url, {headers: headers}).toPromise();

    }

}
