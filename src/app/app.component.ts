import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ActionSheetPage} from "../pages/action-sheet/action-sheet";
import {AlertPage} from "../pages/alert/alert";
import {BadgesPage} from "../pages/badges/badges";
import {BotoesPage} from "../pages/botoes/botoes";
import {CardPage} from '../pages/card/card';
import {CheckboxPage} from '../pages/checkbox/checkbox';
import {RadioPage} from "../pages/radio/radio";
import {RagePage} from "../pages/rage/rage";
import {DatetimePage} from "../pages/datetime/datetime";
import {FabPage} from "../pages/fab/fab";
import {GridPage} from "../pages/grid/grid";
import {IconesPage} from "../pages/icones/icones";
import {InputPage} from "../pages/input/input";
import {ListPage} from "../pages/list/list";
import {TooglePage} from "../pages/toogle/toogle";
import {LoadingPage} from "../pages/loading/loading";
import {ToastPage} from "../pages/toast/toast";
import {ToolbarPage} from "../pages/toolbar/toolbar";
import {SegmentsPage} from './../pages/segments/segments';
import {TabsPage} from '../pages/tabs/tabs';
import {SelectPage} from "../pages/select/select";
import {PaginaPrincipalPage} from "../pages/pagina-principal/pagina-principal";
import {ModalPrincipalPage} from "../pages/modal-principal/modal-principal";
import {SlidesPage} from "../pages/slides/slides";
import {TypografiaPage} from "../pages/typografia/typografia";
import {ChipsPage} from "../pages/chips/chips";
import {PopoversPage} from "../pages/popovers/popovers";
import {EstilizacaoPage} from "../pages/estilizacao/estilizacao";
import {CameraPage} from "../pages/camera/camera";
import {BarcodePage} from "../pages/barcode/barcode";
import {GeolocalizacaoPage} from "../pages/geolocalizacao/geolocalizacao";
import {CepPage} from "../pages/cep/cep";
import {StorePage} from "../pages/store/store";


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage; //Home page

    paginas: Array<{ title: string, component: any }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
        this.initializeApp();

        // used for an example of ngFor and navigation Lista Menu
        this.paginas = [
            {title: 'Home', component: HomePage},
            {title: 'ActionSheet', component: ActionSheetPage},
            {title: 'Alert', component: AlertPage},
            {title: 'Badges', component: BadgesPage},
            {title: 'Botoes', component: BotoesPage},
            {title: 'Card', component: CardPage},
            {title: 'Checkbox', component: CheckboxPage},
            {title: 'Radio', component: RadioPage},
            {title: 'Range', component: RagePage},
            {title: 'DateTime', component: DatetimePage},
            {title: 'Fab', component: FabPage},
            {title: 'Grid', component: GridPage},
            {title: 'Icones', component: IconesPage},
            {title: 'Input', component: InputPage},
            {title: 'List', component: ListPage},
            {title: 'Toogle', component: TooglePage},
            {title: 'Loading', component: LoadingPage},
            {title: 'Toast', component: ToastPage},
            {title: 'Toolbar', component: ToolbarPage},
            {title: 'Segments', component: SegmentsPage},
            {title: 'Tabs', component: TabsPage},
            {title: 'Select', component: SelectPage},
            {title: 'Navegação', component: PaginaPrincipalPage},
            {title: 'Modal', component: ModalPrincipalPage},
            {title: 'Slides', component: SlidesPage},
            {title: 'Typografia', component: TypografiaPage},
            {title: 'Chips', component: ChipsPage},
            {title: 'Popoves', component: PopoversPage},
            {title: 'Estilização', component: EstilizacaoPage},
            {title: 'Camera', component: CameraPage},
            {title: 'Barcode', component: BarcodePage},
            {title: 'Geolocalização', component: GeolocalizacaoPage},
            {title: 'api cep Http', component: CepPage},
            {title: 'Store', component: StorePage}

        ];

    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    abrirPagina(page) {

        //this.nav.setRoot(page.component); //nao tei opcao volta pq pagina e root
        this.nav.push(page.component);//cria pilha de pagina com icon voltar
    }
}
