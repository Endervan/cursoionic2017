import {TabsPage} from './../pages/tabs/tabs';
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from "@ionic/storage";

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ActionSheetPage} from "../pages/action-sheet/action-sheet";
import {AlertPage} from "../pages/alert/alert";
import {BadgesPage} from "../pages/badges/badges";
import {BotoesPage} from "../pages/botoes/botoes";
import {CardPage} from "../pages/card/card";
import {ListPage} from "../pages/list/list";
import {CheckboxPage} from '../pages/checkbox/checkbox';
import {RadioPage} from "../pages/radio/radio";
import {RagePage} from "../pages/rage/rage";
import {DatetimePage} from "../pages/datetime/datetime";
import {FabPage} from "../pages/fab/fab";
import {GridPage} from "../pages/grid/grid";
import {IconesPage} from "../pages/icones/icones";
import {InputPage} from "../pages/input/input";
import {TooglePage} from "../pages/toogle/toogle";
import {LoadingPage} from "../pages/loading/loading";
import {ToastPage} from "../pages/toast/toast";
import {ToolbarPage} from "../pages/toolbar/toolbar";
import {SegmentsPage} from './../pages/segments/segments';
import {SelectPage} from "../pages/select/select";
import {PaginaPrincipalPage} from "../pages/pagina-principal/pagina-principal";
import {Pagina1Page} from "../pages/pagina1/pagina1";
import {Pagina2Page} from "../pages/pagina2/pagina2";
import {ModalPrincipalPage} from "../pages/modal-principal/modal-principal";
import {ModalConteudoPage} from "../pages/modal-conteudo/modal-conteudo";
import {SlidesPage} from "../pages/slides/slides";
import {TypografiaPage} from "../pages/typografia/typografia";
import {ChipsPage} from "../pages/chips/chips";
import {PopoversPage} from "../pages/popovers/popovers";
import {EstilizacaoPage} from "../pages/estilizacao/estilizacao";
import {CameraPage} from "../pages/camera/camera";
import {Camera} from "@ionic-native/camera";
import {BarcodePage} from "../pages/barcode/barcode";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {GeolocalizacaoPage} from "../pages/geolocalizacao/geolocalizacao";
import {Geolocation} from '@ionic-native/geolocation';
import {CepPage} from "../pages/cep/cep";
import {CepProvider} from '../providers/cep/cep';
import {HttpModule} from "@angular/http";
import {StorePage} from "../pages/store/store";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        ActionSheetPage,
        AlertPage,
        BadgesPage,
        BotoesPage,
        CardPage,
        CheckboxPage,
        RadioPage,
        RagePage,
        DatetimePage,
        FabPage,
        GridPage,
        IconesPage,
        InputPage,
        TooglePage,
        LoadingPage,
        ToastPage,
        ToolbarPage,
        SegmentsPage,
        TabsPage,
        SelectPage,
        PaginaPrincipalPage,
        Pagina1Page,
        Pagina2Page,
        ModalPrincipalPage,
        ModalConteudoPage,
        SlidesPage,
        TypografiaPage,
        ChipsPage,
        PopoversPage,
        EstilizacaoPage,
        CameraPage,
        BarcodePage,
        GeolocalizacaoPage,
        CepPage,
        StorePage
    ],
    imports: [
        BrowserModule,
        HttpModule,//import site cep
        IonicModule.forRoot(MyApp),//(MyApp, {mode:'ios'}ou {mode:'md'}) usa msm modelo pra todos
        IonicStorageModule.forRoot() //Storage para armazenar dados
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        ActionSheetPage,
        AlertPage,
        BadgesPage,
        BotoesPage,
        CardPage,
        CheckboxPage,
        RadioPage,
        RagePage,
        DatetimePage,
        FabPage,
        GridPage,
        IconesPage,
        InputPage,
        TooglePage,
        LoadingPage,
        ToastPage,
        ToolbarPage,
        SegmentsPage,
        TabsPage,
        SelectPage,
        PaginaPrincipalPage,
        Pagina1Page,
        Pagina2Page,
        ModalPrincipalPage,
        ModalConteudoPage,
        SlidesPage,
        TypografiaPage,
        ChipsPage,
        PopoversPage,
        EstilizacaoPage,
        CameraPage,
        BarcodePage,
        GeolocalizacaoPage,
        CepPage,
        StorePage
    ],

    //recurso nativo ionic tei coloca aqui
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Camera,
        BarcodeScanner,
        Geolocation,
        CepProvider
    ]
})
export class AppModule {
}
